import React, { Component } from "react";
import PropTypes from "prop-types";
import { Row, Col, Collapse } from "reactstrap";
import onClickOutside from "react-onclickoutside";
import { find, isEmpty } from "lodash";
import { Spinner } from "settour-f2e-components-web";
import "./DestinationMenu.scss";
import newData from "../newData.json";
import _ from 'lodash';
class AirportCityMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      location: [],
      searchTerm: "",
      areaName:"",
      isShow: false,
      collapse: false,
      searchData:[],
    };
    this.late = _.debounce(this.postTerm, 250);
    // this.changeHotelCity = this.changeHotelCity.bind(this);
    // this.createLink = this.createLink.bind(this);
    // this.late = this.late.bind(this);
  }

  componentDidMount =()=>{
     this.setState({
       location:newData
     });
  }

  changeHotelCity = event => {
    this.setState({
      searchTerm: event.target.value
    }, () => {
      this.late(this.state.searchTerm);});
  };

  postTerm = (term) => {
      // Throttled function
        console.log('postterm',term);
         const formData = new FormData();
         formData.append("term", encodeURI(term));
         const xhr = new XMLHttpRequest();
         xhr.open("POST", "", true);
         xhr.send(formData);
         xhr.onload = (res) => {
           if (res.currentTarget.response){
             this.setState({
               searchData: res.currentTarget.response
             },console.log(this.state.searchData))}
             return null;
          }
         };

  searchLink = (result) =>{
    console.log(result);
    if (result) {
      console.log(result);
      // result.map(())

        // (({ displayName, regionId }) => <li key={regionId}>
        //   <div onClick={e => this.selectItem(e)} className="dropdownlink" role="button" tabIndex="0">
        //     {displayName}
        //   </div>
        // </li>);
    }
    return (<li>
              <div
                className="dropdownlink"
                role="button"
                tabIndex="0"
              >
                查無結果
              </div>
           </li>)
  }

  createLink = (data) =>
    {if(data){ 
       return (data.map(({
               areaName,
               areaLocation,
               areaCode
             }) => ( 
             < li key = {areaCode}
        // className={this.state.areaName === areaCode ? "open" : ""}
      >
        {/* {console.log(data)} */}
        <div
          onClick={e => this.selectItem(e)}
          className="dropdownlink"
          role="button"
          tabIndex="0">
          {areaName}
          {
            areaName && this.state.areaName === areaName ?
          (<i className="fa fa-angle-up" aria-hidden="true" />
          ) : (
            <i className="fa fa-angle-down" aria-hidden="true" />
          )}
        </div>
        {
          (
          // <Collapse
          //   isOpen={
          //     this.state.areaName === areaName
          //       ? !this.state.isShow
          //       : this.state.isShow
          //   }
          // >
            <Collapse isOpen = {
              this.state.areaName === areaName ?
              !this.state.isShow :
                this.state.isShow
            }
            className = "sub-menu-items" >
              {areaLocation.map(({ cityName, code }) => {
                return (
                  <li key={code}>
                    <a href="#"
                      onClick={e => {
                        e.preventDefault();
                        this.setState({
                          searchTerm: e.currentTarget.textContent,
                          collapse: false
                        });
                      }}
                    >
                      {cityName}
                    </a>
                  </li>
                );
              })}
            </Collapse>
          // </Collapse>
        )}
      </li>)))}
      return null;
  }

  selectItem = e => {
    if (this.state.areaName !== '') {
      if (this.state.areaName === e.currentTarget.textContent) {
        this.setState(() => ({
          areaName: "",
          isShow: false
        }));
      } else {
        this.setState({
          areaName: e.currentTarget.textContent
        });
      }
    } else {
      this.setState({ areaName: e.currentTarget.textContent });
    }
  };

  handleClickOutside = () => {
    this.setState({ collapse: false, areaName: "",searchData:[] });
  };


  render() {
    const {
      searchTerm,
      location,
      searchData
    } = this.state;
    return (
      <Row>
        <Col xs={3}>
          <div className="form-label">
            <label>目的地</label>
          </div>
        </Col>
        <Col xs={9}>
          <div className="select-location input-box-border form-group">
            <input
              type="text"
              placeholder="目的地"
              className="input form-control"
              value={searchTerm}
              onChange={(e)=>this.changeHotelCity(e)}
              // onFocus={() => {
              //   this.setState({ collapse: true });
              // }}
              onFocus = {
                () => {
                  this.setState({
                    location:newData,
                    collapse: true
                  });
                }
              }
              // ref={input => {
              //   this.input = input;
              // }}
            />
            < Collapse className = "location-list" isOpen = {this.state.collapse} >
              < ul className = "accordion-menu" >
                    {
                      location && this.createLink(location)
                    }
                    {searchTerm && this.searchLink(searchData)}
              </ul>
            </Collapse>
          </div>
        </Col>
      </Row>
    );
  }
}

AirportCityMenu.defaultProps = {
  returnValue: () => {}
};

AirportCityMenu.propTypes = {
  returnValue: PropTypes.func
};

export default onClickOutside(AirportCityMenu);
